import ase
from ase.io import *
import numpy as np
import os
import random

#read the best lowest energy conformer as unique structures
struct =  []
#the range(100) should be changed on your own case
for i in range(100):
    os.chdir('conf_{}'.format(i))
    a = read('crest_best.xyz',index=':',format='xyz')
    struct.extend(a)
    os.chdir('..')


for value, mol in enumerate(struct):
    mol.write('conf_str.xyz',append=True)

