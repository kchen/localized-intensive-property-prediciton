# please refer more on https://github.com/LiamWilbraham/smilescombine

import rdkit, rdkit.Chem as rdkit
from rdkit.Chem import rdMolDescriptors
import itertools

class SpecificationError(Exception):
    def __init__(self, message):
        self.message = message


class Combiner:
    def __init__(self, skeleton, substituents, nmax=2, nconnect=0,
                 connect_atom='Br', auto_placement=True):

        self.skeleton_smiles = skeleton
        self.substituents = substituents#self.assign_ring_order(skeleton, substituents)
        self.nmax = nmax
        self.nconnect = nconnect
        self.connect_atom = connect_atom
        self.auto_placement = auto_placement
        self.combinations = []


    def combine_substituents(self, filename=None, info=True):
        template = self.get_skeleton_template()

        for smiles in self.get_substituent_permutations(template):
            if smiles not in self.combinations:
                self.combinations.append(smiles)

        self.combinations = sorted(self.combinations, reverse=True)
        self.n_combinations = len(self.combinations)

        if info:
            print('Skeleton SMILES:', self.skeleton_smiles)
            print('Number of vacant sites:', self.vacant_sites)
            print('Numer of unique substituent permutations:', self.n_combinations, '\n')

        if filename is not None:
            self.write_smiles(filename, self.combinations)

        return self.combinations

    def get_substituent_permutations(self, template):
        if self.nmax is not None:
            if self.nmax >=self.vacant_sites:
                vacancies = self.vacant_sites - self.nconnect
            else:
                vacancies = self.nmax - self.nconnect
        else:
            vacancies = self.vacant_sites - self.nconnect

        for i in range(vacancies+1):
            combinations = itertools.product(self.substituents, repeat=i)

            combinations = [(list(i) + ['('+self.connect_atom+')']*self.nconnect) for i in combinations]
            combinations = [i+['']*(self.vacant_sites - len(i)) for i in combinations]
            #combinations = self.assign_ring_order(combinations)

            for combination in combinations:
                for permutation in set(itertools.permutations(combination)):
                    smiles = rdkit.MolToSmiles(rdkit.MolFromSmiles(template.format(*permutation)), canonical=True)
                    yield smiles


    def get_skeleton_template(self):
        if self.auto_placement:
            mol_h = rdkit.MolFromSmiles(self.skeleton_smiles)
            rdkit.AddHs(mol_h)
            template = rdkit.MolToSmiles(mol_h, allHsExplicit=True)
            template = template.replace('[cH]', 'c{}').replace('[c]', 'c')
            template = template.replace('[CH]', 'C{}').replace('[C]', 'C')
        else:
            template = self.skeleton_smiles.replace('(Br)', '{}')

        self.vacant_sites = template.count('{}')


        if self.nconnect > self.vacant_sites:
            raise SpecificationError(
                "Number of connections cannot be greater than the number of possible substitution sites.")
        if self.nmax is not None:
            if self.nconnect > self.nmax:
                raise SpecificationError(
                    "Number of connections cannot be greater than the maximum number of allowed substitutions.")

        return template


    def assign_ring_order(self, skeleton, substituents):

        n = rdMolDescriptors.CalcNumAromaticRings(rdkit.MolFromSmiles(skeleton))

        for i, item in enumerate(substituents):
            rings = rdMolDescriptors.CalcNumAromaticRings(rdkit.MolFromSmiles(item[1:-1]))
            if rings > 0:
                for j in reversed(range(rings+1)):
                    item = item.replace(str(j), str(j+n))
                substituents[i] = item


        return substituents


    def write_smiles(self, filename, combinations):

        """
        Writes SMILES to *.csv file
        """

        with open(filename, 'w') as f:
            for smi in combinations:
                f.write(smi + '\n')


    def __str__(self):
        string = 'Skeleton SMILES: ' + self.skeleton_smiles + '\n'
        string += 'Substituents: ' + str(self.substituents) + '\n'
        string += 'Max number of substitutions: ' + str(self.nmax) + '\n'
        string += 'Possible substitution sites: ' + str(self.vacant_sites) + '\n'
        string += 'Number of unique combinations: ' + str(len(self.combinations)) + '\n'
        if self.nconnect > 0:
            string += 'Connection points: ' + str(self.nconnect) + '\n'
        return string


    def __repr__(self):
        return str(self)


