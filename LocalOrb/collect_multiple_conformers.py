import ase
from ase.io import *
import numpy as np
import os
import random

#collect the lowest energy conformer as well as five random conformers
struct =  []
for i in range(100):
    os.chdir('conf_{}'.format(i))
    a = read('crest_conformers.xyz',index=':',format='xyz')
    #if the conformers are smaller than five, then collect them all
    if len(a) <= 5:
        struct.extend(a)
    else:
        random.seed(i)
        #random choose five conformers beside the lowest energy conformer
        ind = random.sample(range(1, len(a)), 5)
        index = [0] + ind
        print(index)
        struct.extend(a[i] for i in index)
    os.chdir('..')

for value, mol in enumerate(struct):
    mol.write('conf_5str.xyz',append=True)



