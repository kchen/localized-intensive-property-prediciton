import os
from ase.atoms import Atoms
import ase
from ase.io import *
import time
import numpy as np


mol = read('str.xyz',index=':',format='xyz')

#first optimize the inital strcutre by gfn2
#then using optmized structure as input for crest to generate conformers
for i, value in enumerate(mol):
    os.mkdir('conf_{}'.format(i))
    os.chdir('conf_{}'.format(i))
    value.write('single.xyz')
    os.system('xtb single.xyz --gfn2 --opt')
    os.system('crest xtbopt.xyz -gfn2 -T 72')
    os.chdir('..')


