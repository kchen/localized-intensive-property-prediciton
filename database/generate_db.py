from ase.io import *
from schnetpack import AtomsData
import numpy as np

#read the xyz file 
mols = read('optmized.xyz',index=':',format='xyz')

property_list = []
for at in mols:
    hm = np.array([at.info['N_HOMO']], dtype=np.float32) #HOMO energy in eV
    hm_au = np.array([at.info['N_HOMO']/27.212],dtype=np.float32) #HOMO energy in au
    lm = np.array([at.info['N_LUMO']], dtype=np.float32) #LUMO energy in eV
    gap = np.array([at.info['N_GAP']], dtype=np.float32) #LUMO energy in au
    IP = np.array([at.info['Ionization_potential']], dtype=np.float32) #Ionization potentail energy in eV
    IP_au = np.array([at.info['Ionization_potential']/27.212],dtype=np.float32)#Ionization potentail energy in au
    coeff = np.array(at.info['HOMO_coeff_r']) #Ionization potentail energy in eV  atomic orbital fraction 
    property_list.append({'HOMO':hm,'HOMO_au':hm_au,'LUMO':lm,'GAP':gap,'IP':IP, 'IP_au':IP_au,'coeff':coeff})

#defined a data file name
new_dataset = AtomsData('./dataset_coeff_conformer.db',available_properties=['HOMO','HOMO_au','LUMO','GAP','IP','IP_au','coeff'])
new_dataset.add_systems(mols,property_list)
