#!/usr/bin/env python

"""
ORCA orbital parser

Juha Siitonen 5.4.2020
"""

import os
import sys
import argparse
import re

def readInOrbitals(file):
    orbitalSectionFlag = 0
    orbitals = []
    orbitals_all = []
    idx = 0
    for line in file:
        if not orbitalSectionFlag:
            if(re.search("\s*NO\s*OCC\s*E\(Eh\)\s*E\(eV\)\s*", line)):
                orbitalSectionFlag = 1
        else:
            #if(line == "------------------"):
            if idx>280:  #this value shoult not exceed the number of orbitals, sometimes we should change this value a bit
                break;
        if orbitalSectionFlag:
            idx += 1
            orbital = line.split()
            orbitals.append({
                'no': orbital[0],
                'occupancy': orbital[1],
                'eh': orbital[2],
                'ev': orbital[3]
                })

        # remove the off-by-one
    return orbitals[1:]


def main(arguments):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('infile', help="Input file", type=argparse.FileType('r'))

    args = parser.parse_args(arguments)

    try:
        fh = open(args.infile.name,"r")
    except IOError:
        print("Error: can\'t find file or read data")

    orbitals = readInOrbitals(fh)

    fh.close()

    occupied = [orb for orb in orbitals if orb['occupancy']=="2.0000"]

    homo = occupied[-1]


    print(homo["ev"])
    lumo = [orb for orb in orbitals if orb['occupancy']=="0.0000"][0]
    print(lumo['ev'])

    print(float(homo["ev"])-float(lumo["ev"]))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))



