import numpy as np
import pandas as pd
from ase.io import *
import ase
import os
from calculate_orbital_fraction import coefficient,coefficient_1

structure = []

#go to orca output to extract the results
for index in range(5000):
    os.chdir('orca_{}'.format(index))
    print(index)
    xyz1 = read('str.xyz',format='xyz') #read the optmized structure in orca calculation folder
    os.chdir('n') # read the neutral single-point calculation results
    os.system('python orbital_extract_orca.py orca.out > orbital')
    os.system('grep \'FINAL SINGLE POINT ENERGY\' orca.out | awk \'{print $NF}\' > Energy')
    L = len(xyz1) + 1
    os.system('grep -i -A {} \'MULLIKEN ATOMIC CHARGES\' orca.out | tail -{} > Mu_charge'.format(L,len(xyz1)))
    os.system('grep -i -A {} \'LOEWDIN ATOMIC CHARGES\' orca.out | tail -{} > LO_charge'.format(L,len(xyz1)))
    os.system('grep \'Total Dipole Moment\' orca.out | tail -3 > dipole')

    with open('orbital') as out:
        L = []
        for line in out.readlines():
            L.append(line)
        homo = L[0]
        lumo = L[1]
        gap = L[2]
    xyz1.info['N_HOMO'] = float(homo)
    xyz1.info['N_LUMO'] = float(lumo)
    xyz1.info['N_GAP'] = float(gap)

    with open('Energy') as out:
        for line in out.readlines():
            energy = line
    xyz1.info['N_Energy'] = float(energy)

    import cclib
    import numpy as np
    filename = 'orca.out'
    data = cclib.io.ccread(filename)
    ratio = coefficient(data) # get the orbital fraction values
    xyz1.info['HOMO_coeff_r'] =  ratio
    homo1_ratio = coefficient_1(data)
    xyz1.info['HOMO1_coeff_r'] = homo1_ratio

    Charge_m = []
    with open('Mu_charge') as out:
        for line in out.readlines():
            q = line.split(':')[1]
            Charge_m.append(float(q))
        Charge_m = np.hstack(Charge_m)
    xyz1.arrays.update({'N_Muliken_charge':Charge_m})

    Charge_l = []
    with open('LO_charge') as out:
        for line in out.readlines():
            q = line.split(':')[1]
            Charge_l.append(float(q))
        Charge_l = np.hstack(Charge_l)
    xyz1.arrays.update({'N_LOEWDIN_charge':Charge_m})
    os.chdir('..')
    os.chdir('n_c')
    os.system('grep \'FINAL SINGLE POINT ENERGY\' orca.out | awk \'{print $NF}\' > Energy')

    with open('Energy') as out:
        for line in out.readlines():
            energy_nc = line
    xyz1.info['NC_Energy'] = float(energy_nc)

    IP = (float(energy_nc) - float(energy)) * 27.2113961318065

    xyz1.info['Ionization_potential'] = IP

    os.chdir('..')
    os.chdir('..')
    structure.append(xyz1)

for i,atom in enumerate(structure):
    atom.write('optmized.xyz',append=True)



