import cclib
import numpy as np


def coefficient(data): #generate the HOMO orbital the orbital fraction based on atoms
    p = []
    coeff = data.mocoeffs
    basis = data.atombasis
    for i, value in enumerate(basis):
        index = data.homos[0] #homo index
        HOMO_coeff = coeff[0][index]
        #coefficient for each atom
        atom_coeff = HOMO_coeff[value]
        #normailization
        r = np.sum(np.square(atom_coeff)) / np.sum(np.square(HOMO_coeff))
        p.append(r)
    return p

def coefficient_1(data): #generate the HOMO-1 orbital the orbital fraction based on atoms
    p = []
    coeff = data.mocoeffs
    basis = data.atombasis
    for i, value in enumerate(basis):
        index = data.homos[0] #homo index
        HOMO1_coeff = coeff[0][index-1]
        #coefficient for each atom
        atom1_coeff = HOMO1_coeff[value]
        #normailization
        r = np.sum(np.square(atom1_coeff)) / np.sum(np.square(HOMO1_coeff))
        p.append(r)
    return p
