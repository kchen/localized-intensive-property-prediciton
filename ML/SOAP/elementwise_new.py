import argparse
import logging
import os
import sys
import numpy as np
from shutil import copyfile, rmtree
from ase import units
import torch
import torch.nn as nn
from torch.optim import Adam
from torch.utils.data.sampler import RandomSampler

import schnetpack as spk
from schnetpack import AtomsData
from schnetpack.utils import get_loaders


from dscribe.descriptors import SOAP
from ase import Atoms
from dscribe.descriptors import Descriptor
from dscribe.core import System
from dscribe.utils.dimensionality import is1d
import dscribe.ext


from schnetpack.nn.base import Dense
from schnetpack import Properties
from schnetpack.nn.cfconv import CFConv
from schnetpack.nn.cutoff import CosineCutoff
from schnetpack.nn.acsf import GaussianSmearing
from schnetpack.nn.neighbors import AtomDistances
from schnetpack.nn.activations import shifted_softplus
import asaplib
from asaplib.data import ASAPXYZ
from asaplib.hypers import universal_soap_hyper
from ase.io import *
from sklearn import preprocessing


def norm_new(value):
    N = []
    for i in range(len(value)):
        std_scale = preprocessing.StandardScaler().fit(value[i])
        X_train_std = std_scale.transform(value[i])
        N.append(X_train_std)
    return N

def padding(S,l):
    x = l - S.size(1)
    padding = (0,x)
    value = 0
    pad = nn.ConstantPad2d(padding, value)
    return pad(S)

class soap_nn(nn.Module):
    def __init__(
        self,
        species=[1, 6, 7, 8, 9, 16, 17, 35],
        method='minimal'
    ):
        super(soap_nn,self).__init__()
        self.species = species
        self.method = method

        self.n_elem = len(self.species)
        self.soap_spec = universal_soap_hyper(self.species, self.method, dump=False)
        if self.method == 'minimal':
            nmax = 4
            lmax = 3
        else:
            nmax = 8
            lmax = 4
       
        for k in self.soap_spec.keys():
            self.soap_spec[k]['rbf'] = 'gto'
            self.soap_spec[k]['crossover'] = True # if crossover=False, self.get_number_of_features = int(self.n_elem * nmax * (nmax + 1) / 2 * (lmax + 1))
        self.n_elem_radial = self.n_elem * nmax
        self.get_number_of_features = int((self.n_elem_radial) * (self.n_elem_radial + 1) / 2 * (lmax + 1))

    def forward(self,inputs):
        atomic_numbers = inputs[Properties.Z].cpu()
        positions = inputs[Properties.R].cpu()
        atom_mask = inputs[Properties.atom_mask]
        Atomic_number = [atomic_numbers[i][atom_mask[i].bool()] for i in range(len(atomic_numbers))]
        Positions = [positions[i][atom_mask[i].bool()] for i in range(len(atomic_numbers))]
        molecules = [Atoms(a,positions=b) for a,b in zip(Atomic_number,Positions)]
        os.system('rm str.xyz')
        for i,mol in enumerate(molecules): #generate xyz for each batch structures
            mol.write('str.xyz',append=True)
        asapxyz = ASAPXYZ('str.xyz', periodic=False)
        # compute atomic descriptors only
        asapxyz.compute_atomic_descriptors(desc_spec_dict=self.soap_spec,
                                           sbs=[],
                                           tag='ourdata',
                                           n_process=72)
        Y  = [asapxyz.atomic_desc[i]['soap1']['atomic_descriptors'] for i in range(len(asapxyz.atomic_desc))]
        y_norm = norm_new(np.array(Y))
        y_torch = [padding(torch.Tensor(y),self.get_number_of_features) for y in y_norm]
        y_last = torch.nn.utils.rnn.pad_sequence(y_torch, batch_first=True)
        return y_last.cuda()


class ElementalAtomwise_coeff(nn.Module):
    def __init__(
        self,
        n_out=2,
        n_layers=2,
        n_neurons=None,
        activation=spk.nn.activations.shifted_softplus,
        property='HOMO_au',
        contributions=None,
        derivative=None,
        negative_dr=False,
        stress=None,
        create_graph=False,
        mean=None,
        stddev=None,
        atomref=None,
        outnet=None,
    ):
        super(Atomwise_new,self).__init__()

        self.n_layers = n_layers
        self.create_graph = create_graph
        self.property = property
        self.contributions = contributions
        self.derivative = derivative
        self.negative_dr = negative_dr
        self.stress = stress

        mean = torch.FloatTensor([0.0]) if mean is None else mean
        stddev = torch.FloatTensor([1.0]) if stddev is None else stddev
        #for homo prediction,   the elements should change depends on system
        self.outnet = schnetpack.nn.blocks.GatedNetwork(representation.get_number_of_features,n_out,elements=frozenset((1, 6, 7, 8, 9, 16, 17, 35)),n_layers=5,activation=activation,) #here should correspond to the element in your dataset


        self.outnet1 = schnetpack.nn.blocks.GatedNetwork(representation.get_number_of_features,n_out,elements=frozenset((1, 6, 7, 8, 9, 16, 17, 35)),n_layers=5, activation=activation,)#here should correspond to the element in your dataset

        self.standardize = schnetpack.nn.base.ScaleShift(mean, stddev)
        self.atom_pool = schnetpack.nn.base.Aggregate(axis=1, mean=False)

    def forward(self,inputs):
        atomic_numbers = inputs[Properties.Z]
        atom_mask = inputs[Properties.atom_mask]
        Index = inputs['coeff']
        y2 = self.outnet(inputs)
        y = self.standardize(y2)
        y = torch.sum(y * Index.unsqueeze(-1),axis=1)
        results = {self.property: y}

        return results


class ElementalAtomwise_WA(nn.Module):
    def __init__(
        self,
        n_out=2,
        n_layers=2,
        n_neurons=None,
        activation=spk.nn.activations.shifted_softplus,
        property='HOMO_au',
        contributions=None,
        derivative=None,
        negative_dr=False,
        stress=None,
        create_graph=False,
        mean=None,
        stddev=None,
        atomref=None,
        outnet=None,
    ):
        super(Atomwise_new,self).__init__()

        self.n_layers = n_layers
        self.create_graph = create_graph
        self.property = property
        self.contributions = contributions
        self.derivative = derivative
        self.negative_dr = negative_dr
        self.stress = stress

        mean = torch.FloatTensor([0.0]) if mean is None else mean
        stddev = torch.FloatTensor([1.0]) if stddev is None else stddev
        #for homo prediciton, the elements should change depends on system
        self.outnet = schnetpack.nn.blocks.GatedNetwork(representation.get_number_of_features,n_out,elements=frozenset((1, 6, 7, 8, 9, 16, 17, 35)),n_layers=5,activation=activation,)


        self.outnet1 = schnetpack.nn.blocks.GatedNetwork(representation.get_number_of_features,n_out,elements=frozenset((1, 6, 7, 8, 9, 16, 17, 35)),n_layers=5, activation=activation,)

        self.standardize = schnetpack.nn.base.ScaleShift(mean, stddev)
        self.atom_pool = schnetpack.nn.base.Aggregate(axis=1, mean=False)


    def forward(self,inputs):
        atomic_numbers = inputs[Properties.Z]
        atom_mask = inputs[Properties.atom_mask]
        y1 = self.outnet1(inputs)
        weight = softmax(y1,atom_mask)
        #print('weight',weight)
        y2 = self.outnet(inputs)
        y = self.standardize(y2)
        y = torch.sum(y * weight,axis=1)
        results = {self.property: y,'weight':weight}

        return results

def softmax(x,mask):
    exp_input = torch.exp(x)

    # Set the contributions of "masked" atoms to zero
    if mask is not None:
            # If the mask is lower dimensional than the array being masked,
            #  inject an extra dimension to the end
        if mask.dim() < x.dim():
            mask = torch.unsqueeze(mask, -1)
        exp_input = torch.where(mask > 0, exp_input, torch.zeros_like(exp_input))

        # Sum exponentials along the desired axis
    exp_input_sum = torch.sum(exp_input, axis=1, keepdim=True)

        # Normalize the exponential array by the
    weights = exp_input / exp_input_sum
    return weights




