import numpy as npz

def calc_RMSE(predict,ref):
    err = (predict-ref)
    RMSE = np.sqrt(np.mean(err**2))
    return RMSE


