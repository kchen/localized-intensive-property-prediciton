import argparse
import logging
import os
import sys
import numpy as np
from shutil import copyfile, rmtree
from ase import units
import torch
import torch.nn as nn
from torch.optim import Adam
from torch.utils.data.sampler import RandomSampler

import schnetpack as spk
from schnetpack import AtomsData
from schnetpack.utils import get_loaders


from schnetpack.nn.base import Dense
from schnetpack import Properties
from schnetpack.nn.cfconv import CFConv
from schnetpack.nn.cutoff import CosineCutoff
from schnetpack.nn.acsf import GaussianSmearing
from schnetpack.nn.neighbors import AtomDistances
from schnetpack.nn.activations import shifted_softplus
from schnetpack import Properties


class Atomwise_coeff(nn.Module):
    def __init__(
        self,
        n_in,
        n_out=2,
        n_layers=2,
        n_neurons=None,
        activation=spk.nn.activations.shifted_softplus,
        property='HOMO_au',
        contributions=None,
        derivative=None,
        negative_dr=False,
        stress=None,
        create_graph=False,
        mean=None,
        stddev=None,
        atomref=None,
        outnet=None,
    ):
        super(Atomwise_new,self).__init__()

        self.n_layers = n_layers
        self.create_graph = create_graph
        self.property = property
        self.contributions = contributions
        self.derivative = derivative
        self.negative_dr = negative_dr
        self.stress = stress

        mean = torch.FloatTensor([0.0]) if mean is None else mean
        stddev = torch.FloatTensor([1.0]) if stddev is None else stddev
        #for homo prediciton
        self.outnet = nn.Sequential(
            schnetpack.nn.base.GetItem("representation"),
            schnetpack.nn.blocks.MLP(n_in,n_out,n_neurons,n_layers, activation),
        )
        self.standardize = schnetpack.nn.base.ScaleShift(mean, stddev)
        self.standardize_k = schnetpack.nn.base.ScaleShift(torch.FloatTensor([1.75]),torch.FloatTensor([0.2]))
        self.atom_pool = schnetpack.nn.base.Aggregate(axis=1, mean=False)


    def forward(self,inputs):
        atomic_numbers = inputs[Properties.Z]
        atom_mask = inputs[Properties.atom_mask]
        Index = inputs['coeff']
        y2 = self.outnet(inputs)
        y = self.standardize(y2)
        y = torch.sum(y * Index.unsqueeze(-1),axis=1)
        results = {self.property: y}

        return results




class Atomwise_WA(nn.Module):
    def __init__(
        self,
        n_in,
        n_out=2,
        n_layers=2,
        n_neurons=None,
        activation=spk.nn.activations.shifted_softplus,
        property='HOMO_au',
        contributions=None,
        derivative=None,
        negative_dr=False,
        stress=None,
        create_graph=False,
        mean=None,
        stddev=None,
        atomref=None,
        outnet=None,
    ):
        super(Atomwise_new,self).__init__()

        self.n_layers = n_layers
        self.create_graph = create_graph
        self.property = property
        self.contributions = contributions
        self.derivative = derivative
        self.negative_dr = negative_dr
        self.stress = stress

        mean = torch.FloatTensor([0.0]) if mean is None else mean
        stddev = torch.FloatTensor([1.0]) if stddev is None else stddev
        #for homo prediciton
        self.outnet = nn.Sequential(
            schnetpack.nn.base.GetItem("representation"),
            schnetpack.nn.blocks.MLP(n_in,n_out,n_neurons,n_layers, activation),
        )
        #for atomic coeff prediciton
        self.outnet1 = nn.Sequential(
            schnetpack.nn.base.GetItem("representation"),
            schnetpack.nn.blocks.MLP(n_in,1,n_neurons,n_layers, activation),
        )

        self.standardize = schnetpack.nn.base.ScaleShift(mean, stddev)
        #self.standardize_k = schnetpack.nn.base.ScaleShift(torch.FloatTensor([1.75]),torch.FloatTensor([0.2]))
        self.atom_pool = schnetpack.nn.base.Aggregate(axis=1, mean=False)
    def forward(self,inputs):
        atomic_numbers = inputs[Properties.Z]
        atom_mask = inputs[Properties.atom_mask]
        y1 = self.outnet1(inputs)
        weight = softmax(y1,atom_mask)
        y2 = self.outnet(inputs)
        y = self.standardize(y2)
        y = torch.sum(y * weight,axis=1)
        results = {self.property: y,'weight':weight}

        return results


def softmax(x,mask):
    exp_input = torch.exp(x)

    # Set the contributions of "masked" atoms to zero
    if mask is not None:
            # If the mask is lower dimensional than the array being masked,
            #  inject an extra dimension to the end
        if mask.dim() < x.dim():
            mask = torch.unsqueeze(mask, -1)
        exp_input = torch.where(mask > 0, exp_input, torch.zeros_like(exp_input))

        # Sum exponentials along the desired axis
    exp_input_sum = torch.sum(exp_input, axis=1, keepdim=True)

        # Normalize the exponential array by the
    weights = exp_input / exp_input_sum
    return weights




