import argparse
import logging
import os
import sys
import numpy as np
from shutil import copyfile, rmtree
from ase import units
import torch
from torch.optim import Adam
from torch.utils.data.sampler import RandomSampler

import schnetpack as spk
from schnetpack import AtomsData
from schnetpack.utils import get_loaders
from schnetpack.train import build_mse_loss
from schnetpack.train import Trainer, CSVHook, ReduceLROnPlateauHook
from schnetpack.train.metrics import MeanAbsoluteError

from atomwise_new import Atomwise_WA
from general import calc_RMSE

def mse_loss(batch,result):
    err_sq = 0.1 * torch.nn.functional.mse_loss(batch['HOMO'],result['HOMO']) +  torch.nn.functional.mse_loss(batch['coeff'],result['weight'].squeeze(-1))
    return err_sq

dataset = AtomsData('dataset_coeff_conformer.db',available_properties=['HOMO','HOMO_au','LUMO','GAP','IP','IP_au','coeff'])
train, val, test = spk.train_test_split(
   dataset, 15000, 2000, os.path.join('.', "split.npz")
)
train_loader = spk.AtomsLoader(train, batch_size=50, num_workers=10, shuffle=True,pin_memory=True)
val_loader = spk.AtomsLoader(val, batch_size=50, num_workers=10, pin_memory=True)

properties = ['HOMO','HOMO_au','LUMO','GAP','IP','IP_au','coeff']

means, stddevs = train_loader.get_statistics(["HOMO"])

representation = spk.representation.SchNet(n_atom_basis=128, n_filters=128, n_gaussians=64, n_interactions=6, cutoff=5, cutoff_network=spk.nn.cutoff.CosineCutoff)
property_out = Atomwise_WA(n_in=128,n_out=1,property="HOMO",mean=means["HOMO"],stddev=stddevs["HOMO"],n_layers=4)
model = spk.atomistic.AtomisticModel(representation, property_out)

optimizer = Adam(params=model.parameters(), lr=1e-3)

model_dir = "test_epoch_avg1"  # directory that will be created for storing model
os.makedirs(model_dir)


metrics = [MeanAbsoluteError(p, p) for p in ['HOMO']]
hooks = [CSVHook(log_path=model_dir, metrics=metrics), ReduceLROnPlateauHook(optimizer,patience=5, factor=0.8, min_lr=1e-6,stop_after_min=True)]

trainer = Trainer(
    model_dir,
    model=model,
    hooks=hooks,
    loss_fn=mse_loss,
    optimizer=optimizer,
    train_loader=train_loader,
    validation_loader=val_loader,
)

device = "cuda"
trainer.train(device=device, n_epochs=200)
best_model = torch.load(os.path.join('./test_epoch_avg1', 'best_model'))

test_loader = spk.AtomsLoader(test,batch_size=50, num_workers=10, pin_memory=True)

results_pred_t = []
results_ref_t = []
coeff_t = [] #pedicted atomic weights
max_index_t = [] #dft level atomic coefficient
for count, batch in enumerate(test_loader):
    # move batch to GPU, if necessary
    batch = {k: v.to(device) for k, v in batch.items()}
    # apply model
    pred = best_model(batch)
    results_pred_t.append(pred['HOMO'].cpu().data.numpy())
    coeff_t.append(batch['coeff'].cpu().data.numpy())
    max_index_t.append(np.squeeze(pred['weight'].cpu().data.numpy()))
    results_ref_t.append(batch['HOMO'].cpu().data.numpy())

