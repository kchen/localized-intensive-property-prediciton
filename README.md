# Physics-Inspired Machine Learning of Localized Intensive Properties

In this work, we explore different strategies for learning intensive and localized properties, using HOMO energies in organic molecules
as a representative test case. We analyze the pooling functions that atomistic neural networks use to predict molecular properties, and suggest an orbital weighted average (OWA) approach that enables the accurate prediction of orbital energies and locations.


The folder **LocalOrb** contains all the scripts to generate molecules of the LocalOrb dataset. They are based on functionalized alkane and conjugated alkene backbones .

The folder **database** contains the scripts to extract electronic properties form ORCA output files and to build the ase databases. Our LocalOrb dataset and OE62 dataset are also saved in this direcotory. 

The folder **ML** contains all the scripts for training and prediciton of atomistic neural networks and carrying out the subsequent prediciton tasks. 

**Setup**

    python 3.8
    Atomic Simulation Environment (ASE) 3.21
    NumPy
    PyTorch 1.9
    SchNetPack 1.0
    Dscribe
    ASAP
After install these necessary packages, the scripts can directly run in the python enviroment to recreate the results in the paper.  
